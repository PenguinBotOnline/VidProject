const express = require('express');
const app = express();
const path = require('path');
const yt = require('./js/yt.js');

app.use(express.static(path.join(__dirname, 'public')));
app.use('/public', (req, res, next) => {
    res.sendFile(path.join(__dirname, 'HTML', 'index.html'));
});

app.use(express.urlencoded());
app.use(express.json());

app.post('/send', async (req, res) => {
    const reqObj = req.body;
    console.log(reqObj.linkReq.link);
    const vidData = await yt.process(reqObj.linkReq.link);

    if (vidData.status == "FAILED") {
        console.log('Error processing link');
    }

    // console.log(vidData.json());
    res.json(vidData);
});

app.listen(3000);
