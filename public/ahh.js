// lol
videojs('vid-player').src({ type: 'video/mp4', src: 'vid/plain-roger-sumatera.mp4' });
videojs('vid-player').preload('auto');
const linkInput = document.getElementById('link-form');
const vidPlayer = document.getElementById('vid-player');
const vidlinkForm = document.getElementById('vidlink-form');
const brbr = document.getElementById('brbr');
const linkDiv = document.getElementById('link-div');
const resDD = document.getElementById('res-dd');
const playButton = document.getElementById('link-button');
const goButton = document.getElementById('choose-button');

let vidLink = "";
let method = "";

function changeVid() {

	if (method == "html") {
		vidLink = linkInput.value;
	}

	if (!vidLink) {
		return;
	}

	videojs('vid-player').src({ type: 'video/mp4', src: vidLink });
	console.log(vidLink);
}

function onTypeDrop(menu) {

	switch (menu.value) {
		case "test":
			linkDiv.classList.remove('link-div');
			linkDiv.classList.add('link-div-zero');
			vidLink = "https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-576p.mp4";
			method = "test";
			break;
		case "yt":
			vidLink = "";
			linkDiv.classList.remove('link-div-zero');
			linkDiv.classList.add('link-div');
			resDD.style = "";
			goButton.style = "";
			// resDD.classList.remove('disappear');
			// goButton.classList.remove('disappear');
			clearYtRes();
			method = "youtube";
			break;
		case "gogoanime":
			vidLink = "";
			console.log("Not implemented");
			break;
		case "html":
			vidLink = "";
			linkDiv.classList.add('link-div-zero');
			linkDiv.classList.remove('link-div-zero');
			resDD.style = "display: none";
			goButton.style = "display: none";
			// resDD.classList.add('disappear');
			// goButton.classList.add('disappear');
			method = "html";
			break;
		default:
			console.log("Switch case broke lmao");
			break;
	}

}

function clearYtRes() {
	const dropdown = document.getElementById('res-dd');

	while (!(dropdown.lastChild.value == "none")) {
		dropdown.removeChild(dropdown.lastChild);
	}
}

function onDownload() {
	const options = {
		method: 'GET',
		mode: 'no-cors'
	};

	fetch(linkInput.value, options).then(res => res.blob()).then(file => {
		const tempUrl = URL.createObjectURL(file);
		const aTag = document.createElement('a');
		aTag.href = tempUrl;
		aTag.download = "";
		document.body.appendChild(aTag);
		aTag.click();
		aTag.remove();
	});
}

async function sendYT() {

	if (!linkInput.value) {
		alert("No url is provided, please provide a youtube url.");
		return;
	}
	playButton.disabled = true;
	goButton.disabled = true;
	const ytLink = linkInput.value;

	clearYtRes();

	await fetch('/send', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			linkReq: {
				method: "youtube",
				link: ytLink
			}
		})
	}).then(async function (vidData) {
		const receivedJson = await vidData.json();

		if (receivedJson.status == 'FAILED') {
			alert('Error processing link (invalid url or youtube-stream-url error)');
			return;
		}

		return receivedJson;
	}).then(vidJson => {
		if (vidJson) {
			for (let i = 0; i < vidJson.data.length; i++) {
				const newOption = document.createElement('option');
				newOption.setAttribute('value', vidJson.data[i].url);
				newOption.innerHTML = vidJson.data[i].quality;
				resDD.appendChild(newOption);
			}
			console.log(vidlinkForm);
		}
	});

	console.log('Done processing respond from server');
	goButton.disabled = false;
	playButton.disabled = false;
	alert('The video is now available to play');
}

function onResDrop(menu) {
	vidLink = menu.value;
}