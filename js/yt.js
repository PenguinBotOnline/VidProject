const Youtube = require('youtube-stream-url');

async function process(data) {
    let playableList = [];
    let countList = 0;
    // printAllFormats(data); used for printing all formats

    let fullRes = {
        status: "NONE",
        data: null
    }

    await Youtube.getInfo({ url: data }).then(vid => {

        if (!vid) {
            fullRes = {
                status: "FAILED",
                data: null
            }
            return fullRes;
        }

        for (let i = 0; i < vid.formats.length; i++) {
            const mimeCache = vid.formats[i].mimeType;
            if (mimeCache == 'video/mp4; codecs="avc1.64001F, mp4a.40.2"' || mimeCache == 'video/mp4; codecs="avc1.42001E, mp4a.40.2"') {
                playableList[countList] = {
                    url: vid.formats[i].url,
                    quality: vid.formats[i].qualityLabel
                }
                countList++;
            }
        }
    });
    // console.log(playableList.length);

    // for (let i = 0; i < playableList.length; i++) {
    //     console.log(playableList[i]);
    // }

    // console.log('done');

    // console.log(playableList);
    //

    if (!(fullRes.status == "FAILED")) {
        fullRes = {
            status: "SUCCESS",
            data: playableList
        }
    }


    // console.log(fullRes);

    return fullRes;
}

function printAllFormats(url) {
    Youtube.getInfo({ url: url }).then(vid => {
        for (let i = 0; i < vid.formats.length; i++) {
            console.log(vid.formats[i]);
        }
    });
}

module.exports = {
    process
}